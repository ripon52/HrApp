-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 21, 2020 at 03:12 PM
-- Server version: 5.7.30-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hr_task`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED DEFAULT NULL,
  `designation_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enter_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leave_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `employee_id`, `designation_id`, `date`, `enter_time`, `leave_time`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2020-06-21', '09:00', NULL, '2020-06-21 08:57:41', '2020-06-21 09:00:57');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isActive` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `isActive`, `created_at`, `updated_at`) VALUES
(1, 'Managing Director', 1, '2020-06-21 06:19:11', '2020-06-21 06:19:11'),
(2, 'HR', 1, '2020-06-21 06:19:11', '2020-06-21 06:19:11'),
(3, 'Employee', 1, '2020-06-21 06:19:11', '2020-06-21 06:19:11');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `join_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `present_address` longtext COLLATE utf8mb4_unicode_ci,
  `permanent_address` longtext COLLATE utf8mb4_unicode_ci,
  `designation_id` bigint(20) UNSIGNED DEFAULT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `avatar`, `gender`, `religion`, `nationality`, `nid`, `dob`, `phone`, `email`, `join_date`, `salary`, `present_address`, `permanent_address`, `designation_id`, `isActive`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'cice@mailinator.net', NULL, 'female', 'krishtian', 'cobig@mailinator.net', 'xunevovur@mailinator.com', '2000-02-25', 'moxev@mailinator.net', 'sisuc@mailinator.net', '2000-09-12', '49000', 'Mollitia nesciunt f', 'Asperiores itaque re', 3, 1, NULL, '2020-06-21 08:57:07', '2020-06-21 08:57:07');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isGet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `isEdit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `prefix`, `route`, `path`, `icon`, `isGet`, `isEdit`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ignition healthCheck', '_ignition', 'ignition.healthCheck', '_ignition/health-check', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(2, 'ignition executeSolution', '_ignition', 'ignition.executeSolution', '_ignition/execute-solution', NULL, 'POST', 'store', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(3, 'ignition shareReport', '_ignition', 'ignition.shareReport', '_ignition/share-report', NULL, 'POST', 'store', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(4, 'ignition scripts', '_ignition', 'ignition.scripts', '_ignition/scripts/{script}', NULL, 'GET', 'edit', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(5, 'ignition styles', '_ignition', 'ignition.styles', '_ignition/styles/{style}', NULL, 'GET', 'edit', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(6, 'user add', 'UserManagement', 'user.add', 'UserManagement/add-user', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(7, 'user view', 'UserManagement', 'user.view', 'UserManagement/view-user', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(8, 'user store', 'UserManagement', 'user.store', 'UserManagement/store-user', NULL, 'POST', 'store', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(9, 'user edit', 'UserManagement', 'user.edit', 'UserManagement/edit-user/{id}', NULL, 'GET', 'edit', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(10, 'user update', 'UserManagement', 'user.update', 'UserManagement/update-user/{id}', NULL, 'POST', 'update', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(11, 'user destroy', 'UserManagement', 'user.destroy', 'UserManagement/destroy-user', NULL, 'POST', 'store', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(12, 'permission add', 'Role-Permission', 'permission.add', 'Role-Permission/add-permission', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(13, 'permission store', 'Role-Permission', 'permission.store', 'Role-Permission/save-permission', NULL, 'POST', 'store', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(14, 'permission edit', 'Role-Permission', 'permission.edit', 'Role-Permission/edit-permission/{id}', NULL, 'GET', 'edit', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(15, 'permission update', 'Role-Permission', 'permission.update', 'Role-Permission/update-permission/{id}', NULL, 'POST', 'update', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(16, 'permission destroy', 'Role-Permission', 'permission.destroy', 'Role-Permission/destroy-permission', NULL, 'POST', 'store', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(17, 'role add', 'Role-Permission', 'role.add', 'Role-Permission/add-role', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(18, 'role view', 'Role-Permission', 'role.view', 'Role-Permission/view-role', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(19, 'role store', 'Role-Permission', 'role.store', 'Role-Permission/store-role', NULL, 'POST', 'store', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(20, 'role edit', 'Role-Permission', 'role.edit', 'Role-Permission/edit-role/{id}', NULL, 'GET', 'edit', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(21, 'role update', 'Role-Permission', 'role.update', 'Role-Permission/update-role/{id}', NULL, 'POST', 'update', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(22, 'role destroy', 'Role-Permission', 'role.destroy', 'Role-Permission/destroy-role', NULL, 'POST', 'store', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(23, 'home', 'admin', 'home', 'admin/home', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(24, 'admin password-change', 'admin', 'admin.password-change', 'admin/change-password', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(25, 'admin update', 'admin', 'admin.update', 'admin/update-admin/{id}', NULL, 'POST', 'update', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(26, 'employee add', 'employee', 'employee.add', 'employee/new-employee', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(27, 'employee edit', 'employee', 'employee.edit', 'employee/edit-employee/{id}', NULL, 'GET', 'edit', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(28, 'employee view', 'employee', 'employee.view', 'employee/view-employee', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(29, 'employee store', 'employee', 'employee.store', 'employee/store-employee', NULL, 'POST', 'store', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(30, 'employee update', 'employee', 'employee.update', 'employee/update-employee/{id}', NULL, 'POST', 'update', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(31, 'employee destroy', 'employee', 'employee.destroy', 'employee/destroy-employee', NULL, 'POST', 'store', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(32, 'employee archive', 'employee', 'employee.archive', 'employee/archive-employee', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(33, 'employee restore', 'employee', 'employee.restore', 'employee/restore-employee/{id}', NULL, 'POST', 'update', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(34, 'attendance add', 'attendance', 'attendance.add', 'attendance/new-attendance', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(35, 'attendance edit', 'attendance', 'attendance.edit', 'attendance/edit-attendance/{id}', NULL, 'GET', 'edit', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(36, 'attendance view', 'attendance', 'attendance.view', 'attendance/view-attendance', NULL, 'GET', 'add', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(37, 'attendance store', 'attendance', 'attendance.store', 'attendance/store-attendance', NULL, 'POST', 'store', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17'),
(38, 'attendance single-update', 'attendance', 'attendance.single-update', 'attendance/single-update-attendance/{id}', NULL, 'POST', 'update', 1, '2020-06-21 06:19:17', '2020-06-21 06:19:17');

-- --------------------------------------------------------

--
-- Table structure for table `menu_roles`
--

CREATE TABLE `menu_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu_id` bigint(20) UNSIGNED DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_roles`
--

INSERT INTO `menu_roles` (`id`, `menu_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(2, 2, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(3, 3, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(4, 4, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(5, 5, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(6, 6, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(7, 7, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(8, 8, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(9, 9, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(10, 10, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(11, 11, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(12, 12, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(13, 13, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(14, 14, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(15, 15, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(16, 16, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(17, 17, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(18, 18, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(19, 19, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(20, 20, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(21, 21, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(22, 22, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(23, 23, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(24, 24, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(25, 25, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(26, 26, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(27, 27, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(28, 28, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(29, 29, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(30, 30, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(31, 31, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(32, 32, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(33, 33, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(34, 34, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(35, 35, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(36, 36, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(37, 37, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(38, 38, 1, '2020-06-21 06:19:21', '2020-06-21 06:19:21'),
(39, 34, 2, NULL, NULL),
(40, 36, 2, NULL, NULL),
(41, 26, 2, NULL, NULL),
(42, 28, 2, NULL, NULL),
(43, 12, 2, NULL, NULL),
(44, 17, 2, NULL, NULL),
(45, 6, 2, NULL, NULL),
(46, 7, 2, NULL, NULL),
(47, 1, 6, NULL, NULL),
(48, 5, 6, NULL, NULL),
(49, 33, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(97, '2014_10_12_000000_create_users_table', 1),
(98, '2014_10_12_100000_create_password_resets_table', 1),
(99, '2020_06_21_074237_create_roles_table', 1),
(100, '2020_06_21_074251_create_role_users_table', 1),
(101, '2020_06_21_074317_foreign_user_id_role_id_at_table_role_users', 1),
(102, '2020_06_21_074331_create_menus_table', 1),
(103, '2020_06_21_074344_create_menu_roles_table', 1),
(104, '2020_06_21_074411_menu_id_role_id_at_table_menu_roles', 1),
(105, '2020_06_21_075019_create_employees_table', 1),
(106, '2020_06_21_075308_create_designations_table', 1),
(107, '2020_06_21_075514_foreign_designation_id_at_table_employees', 1),
(108, '2020_06_21_083528_seed_designations', 1),
(109, '2020_06_21_105209_create_attendances_table', 1),
(110, '2020_06_21_105244_foreign_employee_id_designation_id_at_table_attendances', 1),
(111, '2020_06_21_121623_seed_role', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2020-06-21 06:19:11', '2020-06-21 06:19:11'),
(2, 'Add', '2020-06-21 06:19:11', '2020-06-21 06:19:11'),
(3, 'View', '2020-06-21 06:19:11', '2020-06-21 06:19:11'),
(4, 'Edit', '2020-06-21 06:19:11', '2020-06-21 06:19:11'),
(5, 'Delete', '2020-06-21 06:19:11', '2020-06-21 06:19:11'),
(6, 'x', '2020-06-21 08:36:51', '2020-06-21 08:36:51');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, NULL, NULL),
(2, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `image`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ripon Uddin', NULL, 'rislam252@gmail.com', NULL, '$2y$10$Omq.hXfqWp7R3S.JmBCZle2oStEQZqd/eq6HSiBaK/g.Ocdf2pAaS', NULL, '2020-06-21 06:19:11', '2020-06-21 06:19:11'),
(2, 'Arman', NULL, 'admin@gmail.com', NULL, '$2y$10$.wkmbwHBC38858rytFgxze2MtUQLYknllzdBTjxwB7Ibf6bzJ9yoO', NULL, '2020-06-21 08:41:29', '2020-06-21 08:41:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attendances_employee_id_foreign` (`employee_id`),
  ADD KEY `attendances_designation_id_foreign` (`designation_id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employees_designation_id_foreign` (`designation_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_roles`
--
ALTER TABLE `menu_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_roles_menu_id_foreign` (`menu_id`),
  ADD KEY `menu_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_users_role_id_foreign` (`role_id`),
  ADD KEY `role_users_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `menu_roles`
--
ALTER TABLE `menu_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `role_users`
--
ALTER TABLE `role_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendances`
--
ALTER TABLE `attendances`
  ADD CONSTRAINT `attendances_designation_id_foreign` FOREIGN KEY (`designation_id`) REFERENCES `designations` (`id`),
  ADD CONSTRAINT `attendances_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`);

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_designation_id_foreign` FOREIGN KEY (`designation_id`) REFERENCES `designations` (`id`);

--
-- Constraints for table `menu_roles`
--
ALTER TABLE `menu_roles`
  ADD CONSTRAINT `menu_roles_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`),
  ADD CONSTRAINT `menu_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `role_users`
--
ALTER TABLE `role_users`
  ADD CONSTRAINT `role_users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `role_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
