<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\App;

Route::get('locale/{locale}', function ($locale){
    Session::put('hr_locale',$locale);
    return redirect("/");
});


Route::get('reboot',function (){
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:cache');
    Artisan::call('cache:clear');
});


Route::get('/', function () {
    return view('auth.login');
})->name('/');


Route::get('dynamic-web','backend\WebController@index');
Route::get('assign','backend\WebController@assign');

Auth::routes();


Route::middleware(['CheckPermit','auth'])->group(function (){

    Route::prefix('UserManagement')->group(function (){
        /* Custom Registration Start */
        Route::get('add-user','backend\UserController@index')->name('user.add');
        Route::get('view-user','backend\UserController@index')->name('user.view');
        Route::post('store-user','backend\UserController@store')->name('user.store');
        Route::get('edit-user/{id}','backend\UserController@edit')->name('user.edit');
        Route::post('update-user/{id}','backend\UserController@update')->name('user.update');
        Route::post('destroy-user','backend\UserController@destroy')->name('user.destroy');
        /* Custom Registration Start */
    });

    /* Role & Permission Start */
    Route::prefix('Role-Permission')->group(function (){

        /* Permission Settings  Start
        === We Can Enable it if you wants to add Route information dynamically through Form.
        */
        /*Route::get("add-permission","backend\PermissionController@index")->name('permission.add');
        Route::post("/save-permission","backend\PermissionController@store")->name('permission.store');
        Route::get("/edit-permission/{id}","backend\PermissionController@edit")->name('permission.edit');
        Route::post("/update-permission/{id}","backend\PermissionController@update")->name('permission.update');
        Route::post("/destroy-permission","backend\PermissionController@destroy")->name('permission.destroy');*/
        /* Permission Settings  Start */

        /* Role Create Start  */
        Route::get('add-role','backend\RoleController@index')->name('role.add');
        Route::get('view-role','backend\RoleController@index')->name('role.view');
        Route::post('store-role','backend\RoleController@store')->name('role.store');
        Route::get('edit-role/{id}','backend\RoleController@edit')->name('role.edit');
        Route::post('update-role/{id}','backend\RoleController@update')->name('role.update');
        Route::post('destroy-role','backend\RoleController@destroy')->name('role.destroy');
        /* Role Create End  */
    });
    /* Role & Permission End */

    Route::prefix('admin')->group(function (){
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('change-password','backend\UserController@changePassword')->name('admin.password-change');
        Route::post('update-admin/{id}','backend\UserController@updateAdmin')->name('admin.update');
    });

    Route::prefix('employee')->name('employee.')->group(function (){
        $employeeController = 'backend\EmployeeController@';
        Route::get('/new-employee',$employeeController.'index')->name('add');
        Route::get('/edit-employee/{id}',$employeeController.'edit')->name('edit');
        Route::get('/view-employee',$employeeController.'view')->name('view');
        Route::post('/store-employee',$employeeController.'store')->name('store');
        Route::post('/update-employee/{id}',$employeeController.'update')->name('update');
        Route::post('/destroy-employee',$employeeController.'destroy')->name('destroy');
        Route::get('/archive-employee',$employeeController.'archive')->name('archive');
        Route::post('/restore-employee/{id}',$employeeController.'restore')->name('restore');
    });

    Route::prefix('attendance')->name('attendance.')->group(function (){
        $attendanceController = 'backend\AttendanceController@';
        Route::get('/new-attendance',$attendanceController.'index')->name('add');
        Route::get('/edit-attendance/{id}',$attendanceController.'edit')->name('edit');
        Route::get('/view-attendance',$attendanceController.'view')->name('view');
        Route::post('/store-attendance',$attendanceController.'store')->name('store');
        Route::post('/single-update-attendance/{id}',$attendanceController.'singleUpdate')->name('single-update');
    });
});

