<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name'];

    public function menus()
    {
        return $this->belongsToMany(Menu::class,'menu_roles');
    }

    public function prefixMenus($role_id)
    {
        return MenuRole::query()->where('role_id','=',$role_id)->groupBy('prefix')->get();
    }

}
