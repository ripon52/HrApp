<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';
    protected $fillable = ['name', 'prefix', 'route', 'path', 'icon', 'isGet', 'status'];
}
