<?php
namespace App\Traits;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

trait StoreImageTrait{

    public function ImageStore($file,$path){
        $fileName = Date('Y').'_'.substr(rand(111111,999999),0,6).".".$file->getClientOriginalExtension();
        $file->move($path,$fileName);
        return $fileName;
    }

    public function unlinkImage($path){
        @unlink($path);
    }

}
