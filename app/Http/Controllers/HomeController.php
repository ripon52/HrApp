<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['employees'] = Employee::query()->count();
        $data['attendences'] = Attendance::query()->count();
        $data['today_attendances'] = Attendance::query()->where('date',Carbon::now()->format('Y-m-d'))->count();
        
        return view('dashboard.index');
    }
}
