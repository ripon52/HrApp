<?php

namespace App\Http\Controllers\backend;

use App\Attendance;
use App\Employee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    public function index()
    {


        /* if Form Get Request Found*/
        if (isset($_GET['find_designation_id']))
        {
            $designations = decrypt($_GET['find_designation_id']);

            $data['employees'] = Employee::query()->where('designation_id','=',$designations)->get();
        }else{
            $data['employees'] = null;
        }

        $data['designations'] = Employee::query()
            ->groupBy('designation_id')
            ->get();
        return view('attendance.add-attendance')->with($data);
    }

    public function store(Request $request)
    {

        $this->validate($request,[
            'employee_id'=>'required',
            'enter_time'=>'required',
        ]);


        foreach ($request->employee_id as $key=>$employee)
        {
            $e = Employee::query()->findOrFail($employee);

            Attendance::query()->create([
                'employee_id' =>$e->id,
                'date'=>$request->date,
                'designation_id'=>$e->designation_id,
                'enter_time'=>$request->enter_time[$key],
                'leave_time'=> isset($request->leave_time[$key]) ? $request->leave_time[$key] : null,
            ]);
        }
        session()->flash('success','Attendance saved');
        return redirect()->route('attendance.add');
    }


    public function view()
    {

        /* if Form Get Request Found*/
        if (isset($_GET['find_designation_id']))
        {
            $designations = decrypt($_GET['find_designation_id']);
            $date = $_GET['date'];
            $data['attendances'] = Attendance::query()->where('designation_id','=',$designations)
                ->where('date','=',$date)
                ->get();
        }else{
            $data['attendances'] = Attendance::query()->orderByDesc('id')->get();
        }

        $data['designations'] = Attendance::query()->groupBy('designation_id')->get();

        return view('attendance.view-attendance')->with($data);
    }

    public function edit($id)
    {
        $id = decrypt($id);
        $attendance = Attendance::query()->findOrFail($id);
        return view('attendance.edit-attendance',compact('attendance'));
    }

    public function singleUpdate(Request $request,$id)
    {
        $id = decrypt($id);
        $attendance = Attendance::query()->findOrFail($id);
        $attendance->update([
            'enter_time'=>$request->enter_time,
            'leave_time'=>$request->leave_time,
        ]);

        $employee = Employee::query()->findOrFail($attendance->employee_id);

        session()->flash('success',$employee->name. ' - attendance updated');
        return redirect()->route('attendance.add');

    }

}
