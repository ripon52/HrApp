<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PermissionController extends Controller
{
    public function index()
    {
        $permissions = Menu::query()->wherestatus(1)->orderBy('id','desc')->get();
        return view('permission.add-permission',compact('permissions'));
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'prefix'=>'required',
            'name'=>'required|unique:menus',
            'route'=>'required|unique:menus',
            'path'=>'required',
        ]);
        Menu::query()->create($request->all());
        session()->flash('success','Permission successfully stored in your system');
        return redirect()->route('permission.add');
    }


    public function edit($id)
    {
        $permissions = Menu::query()->wherestatus(1)->orderBy('id','desc')->get();
        $permission = Menu::query()->findOrFail($id);
        return view('permission.edit-permission',compact('permission','permissions'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'prefix'=>'required',
            'name'=>['required',Rule::unique('menus')->ignore($id)],
            'route'=>['required',Rule::unique('menus')->ignore($id)],
            //   'path'=>['required',Rule::unique('menus')->ignore($id)],
        ]);

        Menu::query()->findOrFail($id)->update($request->all());
        session()->flash('success','Permission successfully updated in your system');
        return redirect()->route('permission.add');
    }


    public function destroy(Request $request)
    {
        Menu::query()->findOrfail($request->id)->delete();
    }
}
