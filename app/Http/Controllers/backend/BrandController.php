<?php

namespace App\Http\Controllers\backend;

use App\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BrandController extends Controller
{
    public function index(){
        $brands = Brand::orderBy('name','asc')->get();
        return view('brand.add-brand',compact('brands'));
    }

    public function store(Request $request){
        $this->validate($request,['name'=>'required|unique:brands']);
        Brand::create($request->all());
        session()->flash('success','brand controller stored');
        return redirect()->route('brand.add');
    }

    public function edit($id){
        $data['brand'] = Brand::findOrFail($id);
        $data['brands'] = Brand::orderBy('name','asc')->get();
        return view('brand.edit-brand')->with($data);
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'name'=>['required',Rule::unique('brands')->ignore($id)]
        ]);

        Brand::findOrFail($id)->update($request->all());
        session()->flash('success','Brand name Updated');
        return redirect()->route('brand.add');
    }
}
