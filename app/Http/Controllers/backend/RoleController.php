<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Menu;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        $menus = Menu::query()->orderBy('prefix')->pluck('name','id');
        return view('role.add-role',compact('roles','menus'));
    }


    public function store(Request $request)
    {

        $this->validate($request,[
            'name'=>'required|unique:roles',
            'menu_id'=>'required'
        ],
            [
                'name.required'=>'Role Name must be fill-up',
                'menu_id.required'=>'Opps ! Menu unselected detect. Please select any Menu for Permit',
                'name.unique'=>'Opps ! Role Name already stored. Please try another name as Role Name',
            ]
        );
        Role::create($request->all())->menus()->attach($request->menu_id);
        session()->flash('success','Role successfully created ');
        return redirect()->route('role.add');
    }


    public function edit($id)
    {
        // dd($id);
        $role = Role::query()->findOrFail($id);
        $roles = Role::query()->get();
        $menus = Menu::query()->orderBy('prefix')->pluck('name','id');
        return view('role.edit-role',compact('role','roles','menus'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>['required',Rule::unique('roles')->ignore($id)]
        ]);
        $update = Role::query()->findOrFail($id);
        $update->update(["name"=>$request->name]);
        $update->menus()->sync($request->menu_id);
        session()->flash('success','Role successfully updated');
        return redirect()->route('role.add');
    }


    public function destroy(Request $request)
    {
        $delete = Role::findOrFail($request->id);
        $delete->menus()->detach();
        $delete->delete();

    }
}
