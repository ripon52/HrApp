<?php

namespace App\Http\Controllers\backend;

use App\Designation;
use App\Employee;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRegister;
use App\Http\Requests\EmployeeUpdate;
use Illuminate\Http\Request;
use App\Traits\StoreImageTrait;

class EmployeeController extends Controller
{
    use StoreImageTrait;

    public $path = null;
    public function index()
    {
        $data['designations'] = Designation::query()->pluck('name','id');
        return view('employee.add-employee')->with($data);
    }

    public function view()
    {
        $data['employees'] = Employee::query()->where('isActive','!=',0)->get();
        return view('employee.view-employee')->with($data);

    }

    public function store(EmployeeRegister $request)
    {
        $data = $request->except('avatar');
        if ($request->hasFile('avatar'))
        {
            $this->path = public_path('upload/employee');
            $data['avatar'] = $this->ImageStore($request->file('avatar'),$this->path);
        }
        Employee::query()->create($data);
        session()->flash('success','Employee Registration Complete');
        return redirect()->route('employee.add');
    }

    public function edit($id)
    {
        $id = decrypt($id);
        $data['employee'] = Employee::query()->findOrFail($id);
        $data['designations'] = Designation::query()->pluck('name','id');
        return view('employee.edit-employee')->with($data);
    }

    public function update(EmployeeUpdate $request,$id)
    {
        $id = decrypt($id);
        $employee = Employee::query()->findOrFail($id);

        $data = $request->except('avatar');

        if ($request->hasFile('avatar'))
        {
            /* if exits unlink old one */
            $this->path = public_path('upload/employee');
            if ($employee->avatar !=null && file_exists($this->path.'/'.$employee->avatar)){
                $this->unlinkImage($this->path.'/'.$employee->avatar);
            }

            $data['avatar'] = $this->ImageStore($request->file('avatar'),$this->path);
        }

        $employee->update($data);
        session()->flash('success','Employee Updated');
        return redirect()->route('employee.view');
    }

    public function destroy(Request $request)
    {
        $id = decrypt($request->id);
        $delete = Employee::query()->findOrFail($id);
        $delete->delete();
    }

    public function archive()
    {
        $employees = Employee::query()->onlyTrashed()->get();
        return view('employee.archive-employee',compact('employees'));
    }

    public function restore(Request $request,$id)
    {
        $id = decrypt($id);
        $employee = Employee::query()->withTrashed()->findOrFail($id);
        $employee->update(['deleted_at'=>null]);
        session()->flash('success','Employee Recover successfully.');
        return redirect()->route('employee.view');
    }

}
