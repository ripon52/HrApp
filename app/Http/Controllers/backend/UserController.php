<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function index()
    {
        $users = User::query()->get();
        $roles = Role::pluck('name','id');
        return view('user.add-user',compact('users','roles'));
    }


    public function store(UserRequest $request)
    {
        $request['password'] =bcrypt($request->password);
        User::query()->create($request->except('role_id'))->roles()->attach($request->role_id);
        session()->flash('success','User registration complete.');
        return redirect()->route('user.add');
    }

    public function edit($id)
    {
        $user = User::query()->findOrFail($id);
        $users = User::query()->get();
        $roles = Role::pluck('name','id');
        return view('user.edit-user',compact('users','roles','user'));
    }


    public function update(UserUpdateRequest $request, $id)
    {
        $data = $request->except('role_id');

        $update = User::query()->findOrFail($id);
        $update->update($data);
        $update->roles()->sync($request->role_id);
        session()->flash('success','User Updated.');
        return redirect()->route('user.add');
    }

    public function destroy(Request $request)
    {
        $delete = User::query()->findOrFail($request->id);
        $delete->roles()->detach();
        $delete->delete();
    }




    public function changePassword(){
        $user_id = Auth::id();
        $user = User::query()->findOrFail($user_id);
        return view('user.edit-profile',compact('user'));
    }

    public function updateAdmin(Request $request,$id){
        $this->validate($request,[
            'name'=>'required',
            'password'=>'required',
            'confirm_password'=>'required'
        ]);

        $user = User::query()->findOrFail($id);

        if ($request->password != $request->confirm_password){

            $request['password']=null;
            $this->validate($request,[
                'password'=>'required'
            ],
            [
                'password.required'=>'Password and Confirm password not match. Please input correctly'
            ]);
        }

        $request['password'] = bcrypt($request->password);
        $user->update($request->except('confirm_password'));
        return redirect()->route('home');
    }


}
