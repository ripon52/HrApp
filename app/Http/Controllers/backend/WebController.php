<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Menu;
use App\MenuRole;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index(){
        $routes = \Illuminate\Support\Facades\Route::getRoutes();
        $prefix = [];
        $current = $previews = null;
        $i = 0;
        foreach ($routes as $key=>$route){

            if ($current == null){
                $current = $route->getPrefix();
            }else{
                $previews = $current;
                $current = $route->getPrefix();
            }

            if ($current !=$previews){
                if($route->getPrefix() !='api' && $route->getPrefix() !=null){
                    $prefix_name = implode("",explode("/", $route->getPrefix()));
                    $prefix["prefix"][$i] = $prefix_name;
                    echo " Prefix is : ".$prefix_name ."<br><br>";
                    /* save start */
                    foreach ($routes as $in=>$rout_info){

                        $rout_info_prefix_name = $rout_info;

                        if($rout_info_prefix_name !='api' && $rout_info_prefix_name !=null){

                            $query_prefix_name = implode("",explode("/", $rout_info_prefix_name->getPrefix()));

                            if ($prefix_name == $query_prefix_name) {

                                $exists =  Menu::where('route', $rout_info_prefix_name->getName())->get();
                                if ($exists->count()<=0)
                                {
                                    $str= strlen($query_prefix_name);
                                    $menu = new Menu();
                                    $menu->name =  implode(" ",explode(".", $rout_info_prefix_name->getName()));
                                    $menu->prefix = $prefix_name;
                                    $menu->route = $rout_info_prefix_name->getName();
                                    //$menu->path = substr($rout_info_prefix_name->uri,$str+1);
                                    $menu->path = $rout_info_prefix_name->uri;
                                    $menu->isGet = $rout_info_prefix_name->methods[0];
                                    $isGet = $menu->isGet;
                                    $menu->isEdit = ($isGet == 'GET'
                                        ?
                                         (strpos($rout_info_prefix_name->uri,'{') == true ? 'edit' : 'add')
                                        :
                                         (strpos($rout_info_prefix_name->uri,'{') == true  ? 'update' : 'store')
                                    );

                                    $menu->save();
                                    echo "<li>Prefix name  : ".$query_prefix_name."  Inserted Route Name : "
                                    .$rout_info_prefix_name->getName()." Path Name : ".substr
                                ($rout_info_prefix_name->uri,$str+1)." -- ".$str ."</li>";
                                }
                            }
                        }
                    }
                    /* save end */
                    ++$i;
                }
            }
        }
    }


    public function assign(){
        $menus = Menu::all();
        foreach ($menus as $menu){
            MenuRole::create([
                'role_id'=>1,
                'menu_id'=>$menu->id,
                'name'=>$menu->name,
                'prefix'=>$menu->prefix,
                'route'=>$menu->route,
                'path'=>$menu->path,
                'icon'=>$menu->icon,
                'isGet'=>$menu->isGet,
                'isEdit'=>$menu->isEdit,
            ]);
        }
        dd("Complete");
    }
}
