<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class Localization
{
    public function handle($request, Closure $next)
    {
        App::setLocale(Session::get('hr_locale') ?? 'bn');
        return $next($request);
    }
}
