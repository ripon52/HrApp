<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class CheckPermit
{

    public function handle($request, Closure $next)
    {
        $auth = Auth::user();
        $route= Route::current()->getName();
        if ($auth)
        {
            foreach ($auth->roles as $key=>$role){

                foreach ($role->menus as $menu){
                    if ($menu->route == $route){
                        return $next($request);
                    }
                }
            }
            abort('404');
        }else{
            return redirect('/');
        }

    }
}
