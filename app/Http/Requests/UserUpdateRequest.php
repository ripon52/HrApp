<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('id');
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required',Rule::unique('users')->ignore($id)],
            'role_id' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Opps ! Please fill-up Name Field',
            'email.required'=>'Opps ! Please fill-up email Field',
            'email.unique'=>'Opps ! Your given email id already registered. Please try other email name as email id',
            'password.min'=>'Opps ! According to registration credential you have to put 6 character as minimum password',
            'password.required'=>'Password field must be fill-up',
            'password.confirmed'=>'Password & Confirm-Password not match. Please Input password and Confirm password same',
            'role_id.required'=>"Opps ! Role id blank detected. Please select at least one role for user"
        ];
    }
}
