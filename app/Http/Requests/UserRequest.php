<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'role_id' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Opps ! Please fill-up Name Field',
            'email.required'=>'Opps ! Please fill-up email Field',
            'email.unique'=>'Opps ! Your given email id already registered. Please try other email name as email id',
            'password.min'=>'Opps ! According to registration credential you have to put 6 character as minimum password',
            'password.required'=>'Password field must be fill-up',
            'password.confirmed'=>'Password & Confirm-Password not match. Please Input password and Confirm password same',
            'role_id.required'=>"Opps ! Role id blank detected. Please select at least one role for user"
        ];
    }
}
