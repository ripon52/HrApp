<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'nullable|unique:employees',
            'nid'=>'required|unique:employees',
            'designation_id'=>'required',
            'phone'=>'required',
            'gender'=>'required',
            'religion'=>'required',
            'dob'=>'required',
            'nationality'=>'required',
            'present_address'=>'required',
            'permanent_address'=>'required',
            'join_date'=>'required',
            'salary'=>'required',
        ];
    }
}
