<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Employee extends Model
{
    use SoftDeletes;
    protected $table = 'employees';
    protected $fillable = ['name','join_date','salary', 'avatar', 'gender', 'religion', 'nationality', 'nid', 'dob', 'phone',
        'email', 'present_address', 'permanent_address', 'designation_id', 'isActive', 'deleted_at'];

    public function designation()
    {
        return $this->belongsTo(Designation::class);
    }
}
