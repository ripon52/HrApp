<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuRole extends Model
{
    protected $table = 'menu_roles';
    protected $fillable = ['menu_id', 'role_id','name', 'prefix', 'route', 'path', 'icon', 'isGet', 'isEdit'];

    public function role()
    {
        return $this->belongsTo(Role::class,'role_id');
    }

    public function menu()
    {
        return $this->belongsTo(Menu::class,'menu_id');
    }
}
