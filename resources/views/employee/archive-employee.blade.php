@extends('layouts.admin')
@section('title','ALL Employee - HR Application')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Employee Management</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">View Employee </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->

    <section class="content">
        <div class="container-fluid">
            {{-- Cart Item Start --}}
            <div class="row">
                <!-- ./col -->
                <div class="col-lg-12 col-xs-12">
                    {{-- all users retrive start --}}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">ALL Employee</h3>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-hover" id="dataTable">
                                <thead>
                                <tr>
                                    <th>#SL</th>
                                    <th>Name</th>
                                    <th>Avatar</th>
                                    <th>Designation</th>
                                    <th>Join Date</th>
                                    <th>Salary</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($employees->count()>0)
                                    @foreach($employees as $key=>$employee)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $employee->name }}</td>
                                            <td>
                                                <img src="{{ $employee->avatar !=null ? asset('upload/employee/'
                                                    .$employee->avatar) : asset('upload/noimage.png') }}"
                                                     style="height:
                                                    100px;width: 100px" alt="">
                                            </td>
                                            <td>{{ $employee->designation->name }}</td>
                                            <td>{{ $employee->join_date }}</td>
                                            <td>{{ $employee->salary }}</td>
                                            <td>{{ $employee->isActive }}</td>
                                            <td>
                                                {{ Form::open(['route'=>['employee.restore', encrypt($employee->id)]]) }}
                                                <button type="submit" class="btn btn-info"> <i class="fa
                                                fa-recycle"></i> </button>
                                                {{ Form::close() }}
                                            </td>
</tr>
@endforeach
@endif
</tbody>
</table>
</div>
{{-- all users retrive end --}}
</div>

</div>

</div>


</section>
<!-- /.content -->

@endsection

@section('script')
<script>
$(document).ready(function () {
$("#dataTable").DataTable();
});
</script>
@endsection
