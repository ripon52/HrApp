@extends('layouts.admin')

@section('title','New Employee | HR Application')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Employee Management</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">New Employee </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->

    <section class="content">
        <div class="container-fluid">
            {{-- Cart Item Start --}}
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">New Employee</h3>
                        </div>
                        <div class="card-body">
                            {{ Form::model($employee,['route'=>['employee.update', encrypt($employee->id)],'method'=>'post',
                            'files'=>true]) }}
                            <div class="row">
                                @include('employee.form-employee')
                            </div>
                            {{ Form::submit('Update Employee',['class'=>'btn btn-success']) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <!-- /.content -->

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#dataTable").DataTable();
        });
    </script>
@endsection

