

<div class="col-lg-3">
    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} ">
        {{ Form::label('','Name *') }}
          @if($employee)
            {{ Form::hidden('id',encrypt($employee->id)) }}
          @endif
        {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Ex.John Doe']) }}

        @if($errors->has('name'))
            <span class="strong">
                <strong class="help-block"> {{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-lg-3">
    <div class="form-group @error('nid') has-error @enderror ">
        {{ Form::label('','NID *') }}
        {{ Form::text('nid',null,['class'=>'form-control','placeholder'=>'Ex.John Doe']) }}

        @error('nid')
            <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>


<div class="col-lg-3">
    <div class="form-group @error('avatar') has-error @enderror ">
        {{ Form::label('','Avatar') }}
        {{ Form::file('avatar',['class'=>'form-control','accept'=>'image/*']) }}

        @error('avatar')
        <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>



<div class="col-lg-3">
    <div class="form-group @error('dob') has-error @enderror ">
        {{ Form::label('','Date of Birth *') }}
        {{ Form::date('dob',null,['class'=>'form-control','placeholder'=>'Ex. 1990-04-25']) }}

        @error('dob')
        <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>


<div class="col-lg-3">
    <div class="form-group @error('phone') has-error @enderror ">
        {{ Form::label('','Phone *') }}
        {{ Form::text('phone',null,['class'=>'form-control','placeholder'=>'Ex. +8801**********']) }}

        @error('phone')
        <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="col-lg-3">
    <div class="form-group @error('email') has-error @enderror ">
        {{ Form::label('','Email') }}
        {{ Form::text('email',null,['class'=>'form-control','placeholder'=>'Ex. example@example.com']) }}

        @error('email')
        <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>


<div class="col-lg-3">
    <div class="form-group @error('designation_id') has-error @enderror ">
        {{ Form::label('','Designation *') }}
        {{ Form::select('designation_id',$designations,$employee ? $employee->designation_id : false,['class'=>'form-control']) }}

        @error('designation_id')
        <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>


<div class="col-lg-3">
    <div class="form-group @error('religion') has-error @enderror ">
        {{ Form::label('','Religion *') }}
        {{ Form::select('religion',['islam'=>'Islam','hindu'=>'Hindu','buddha'=>'Buddha','krishtian'=>'Krishtian'],
        $employee ? $employee->religion : false,
        ['class'=>'form-control']) }}

        @error('religion')
        <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>


<div class="col-lg-3">
    <div class="form-group @error('gender') has-error @enderror ">
        {{ Form::label('','Gender *') }}
        {{ Form::select('gender',['male'=>'Male','female'=>'Female','other'=>'Third Gender'],$employee ? $employee->gender : false,
        ['class'=>'form-control']) }}

        @error('gender')
            <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="col-lg-3">
    <div class="form-group @error('nationality') has-error @enderror ">
        {{ Form::label('','Nationality *') }}
        {{ Form::text('nationality',null,['class'=>'form-control','placeholder'=>'Ex.Bangladeshi']) }}

        @error('nationality')
             <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="col-lg-3">
    <div class="form-group @error('join_date') has-error @enderror ">
        {{ Form::label('','Joining Date *') }}
        {{ Form::date('join_date',null,['class'=>'form-control','placeholder'=>'Ex.2020-06-21']) }}

        @error('join_date')
        <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="col-lg-3">
    <div class="form-group @error('salary') has-error @enderror ">
        {{ Form::label('','Starting Salary *') }}
        {{ Form::number('salary',null,['class'=>'form-control','placeholder'=>'Ex.40,000']) }}

        @error('salary')
        <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>


<div class="col-lg-6">
    <div class="form-group @error('present_address') has-error @enderror ">
        {{ Form::label('','Present Address *') }}
        {{ Form::textarea('present_address',null,['class'=>'form-control','cols'=>10,'rows'=>10]) }}

        @error('present_address')
            <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>



<div class="col-lg-6">
    <div class="form-group @error('permanent_address') has-error @enderror ">
        {{ Form::label('','Permanent Address *') }}
        {{ Form::textarea('permanent_address',null,['class'=>'form-control','cols'=>10,'rows'=>10]) }}

        @error('permanent_address')
        <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
