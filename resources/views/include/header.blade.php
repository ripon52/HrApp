
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route('home') }}" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Contact</a>
        </li>



        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link online" id="online" style="background: #000;color: #fff;"></a>

        </li>
    </ul>


    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar header_search" type="search" placeholder="Search"
                   aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-navbar" type="button">
                    <i class="fas fa-search"></i>
                </button>
            </div>
        </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->

        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <p class="bg-info" style="padding: 5px;">   {{ Auth::user()->name }} </p>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="#" class="dropdown-item">
                    <i class="fas fa-user mr-2"></i> Member Since
                    <span class="float-right text-muted text-sm"> {{ Auth::user()->created_at->diffForHumans()  }}</span>
                </a>


                <a href="#" class="dropdown-item">
                    <i class="fas fa-user mr-2"></i> Register Date
                    <span class="float-right text-muted text-sm"> {{ \Carbon\Carbon::parse(Auth::user()->created_at)->format('D-d-M-Y ') }}</span>
                </a>

                {{ Form::open(['route'=>'logout','method'=>'form-control']) }}
                <a href="#" class="dropdown-item">

                    <p class="">
                    {{ Form::submit('Log Out',['class'=>'btn-danger btn fas fa-sign-out-alt mr-2']) }}

                    </p>
                </a>
                {{ Form::close() }}

            </div>
        </li>
       {{-- <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
                <i class="fas fa-th-large"></i>
            </a>
        </li>--}}
    </ul>
</nav>
