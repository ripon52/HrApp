<li class="nav-item has-treeview">
    <a href="#" class="nav-link active">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
            Dashboard
            <i class="right fas fa-angle-left"></i>
        </p>
    </a>
</li>

{{-- Menu Header [menu-open]--}}

{{-- Menu Option [active]--}}

@php

    $prefix = Request::route()->getPrefix();
    $route  = Route::current()->getName();
   // dd($prefix,$route);
    $auth = \Illuminate\Support\Facades\Auth::user();

@endphp
    
{{-- User Management --}}

@if($auth && $auth->roles->count()>0)
    {{-- Menus --}}
    @foreach($auth->roles as $key=>$role)
        {{-- Role Menus--}}
            @foreach($role->prefixMenus($role->id) as $in=>$menu)

                <li class="nav-item has-treeview {{ $prefix ==$menu->prefix ? 'menu-open' : '' }}">

                    <a href="#" class="nav-link {{  $prefix == $menu->prefix  ? 'active' : '' }}">
                        <i class="nav-icon fas fa-user-astronaut"></i>
                        <p>
                            {{ $menu->prefix }}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                    @php
                        $prefixMenu = \App\MenuRole::query()->where('prefix','=',$menu->prefix)->get()
                    @endphp

                    @if($prefixMenu->count()>0)
                        @foreach($prefixMenu as $p=>$pmenu)
                            @if($pmenu->isGet == 'GET' &&  $pmenu->isEdit == 'add')
                                <li class="nav-item ">
                                    <a href="{{ route($pmenu->route) }}" class="nav-link {{ $route == $pmenu->route ?
                                    'active' : ''
                                    }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>{{ $pmenu->name }}</p>
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    @else

                    @endif
                    </ul>

                </li>

            @endforeach
    @endforeach
@else

@endif




{{-- Role Management --}}

{{--

<li class="nav-item has-treeview {{ $prefix =='/Role-Permission' ? 'menu-open' : '' }}">

    <a href="#" class="nav-link {{ $prefix =='/Role-Permission' ? 'active' : '' }}">
        <i class="nav-icon fas fa-cog"></i>
        <p>
            Role Management
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>

    <ul class="nav nav-treeview">

        <li class="nav-item ">
            <a href="{{ route('role.add') }}" class="nav-link {{ $route == 'role.add' ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Role</p>
            </a>
        </li>

      --}}
{{--  <li class="nav-item ">
            <a href="{{ route('permission.add') }}" class="nav-link {{ $route == 'permission.add' ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Permission</p>
            </a>
        </li>--}}{{--




    </ul>
</li>



<li class="nav-item has-treeview {{ $prefix =='/employee' ? 'menu-open' : '' }}">

    <a href="#" class="nav-link {{ $prefix =='/employee' ? 'active' : '' }}">
        <i class="nav-icon fas fa-users"></i>
        <p>
            Employee Manage
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>

    <ul class="nav nav-treeview">

        <li class="nav-item ">
            <a href="{{ route('employee.add') }}" class="nav-link {{ $route == 'employee.add' ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p> New Employee</p>
            </a>
        </li>

        <li class="nav-item ">
            <a href="{{ route('employee.view') }}" class="nav-link {{ $route == 'employee.view' ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p> ALL Employees</p>
            </a>
        </li>
    </ul>
</li>


<li class="nav-item has-treeview {{ $prefix =='/attendance' ? 'menu-open' : '' }}">

    <a href="#" class="nav-link {{ $prefix =='/attendance' ? 'active' : '' }}">
        <i class="nav-icon fas fa-clipboard"></i>
        <p>
            Attendance Manage
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>

    <ul class="nav nav-treeview">

        <li class="nav-item ">
            <a href="{{ route('attendance.add') }}" class="nav-link {{ $route == 'attendance.add' ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p> New Attendance</p>
            </a>
        </li>

        <li class="nav-item ">
            <a href="{{ route('attendance.view') }}" class="nav-link {{ $route == 'attendance.view' ? 'active' : '' }}">
                <i class="far fa-circle nav-icon"></i>
                <p> ALL Attendance</p>
            </a>
        </li>
    </ul>
</li>

--}}
