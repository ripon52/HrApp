@extends('layouts.admin')
@section('title','Register User - HR Application')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New User</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Register </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            {{-- Cart Item Start --}}
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"> New User </h3>
                        </div>

                        <div class="card-body">
                            {{ Form::model($user=new \App\User(),['route'=>'user.store','method'=>'post','files'=>true]) }}
                            @include('user.form-user')
                            {{ Form::submit('Register User',['class'=>'btn btn-success']) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            <!-- ./col -->
            <div class="col-lg-8 col-xs-12">
                {{-- all users retrive start --}}
                <div class="card">

                    <div class="card-header">
                        <h3 class="card-title"> User's List </h3>
                    </div>

                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#SL</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @include('user.users')
                            </tbody>
                        </table>
                </div>
                {{-- all users retrive end --}}
                </div>

            </div>

        </div> {{-- Row End --}}


    </section>
    <!-- /.content -->

@endsection
