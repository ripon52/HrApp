@php
    $sl =0;
@endphp

@foreach($users as $user)
    <tr>
        <td> {{ ++$sl }}</td>
        <td> {{ $user->name }}</td>
        <td> {{ $user->email }}</td>
        <td>
            @foreach($user->roles as $role)
                <label class="btn btn-info">{{ $role->name }}</label> &nbsp;
            @endforeach
        </td>
        <td>
            <a href="{{ route('user.edit',$user->id) }}" class="fa fa-edit btn btn-success"></a> &nbsp;
            <button type="button" data-id="{{ $user->id }}" data-url="{{ route('user.destroy') }}" class="btn btn-danger erase fa fa-trash"></button>
        </td>
    </tr>
@endforeach
