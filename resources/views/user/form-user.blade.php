 {{ Form::hidden('id',($user ? $user->id : null),['class'=>'form-control']) }}

<div class="form-group  {{ $errors->has('name') ?  'has-error' : '' }}">
    {{ Form::label('','Name') }}
    {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'User Name']) }}
    @if($errors->has('name'))
        <span class="help-block" role="alert">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    {{ Form::label('','Email') }}
    {{ Form::email('email',null,['class'=>'form-control  ','placeholder'=>'Example@email.com']) }}

    @if($errors->has('email'))
        <span class="help-block" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif

</div>

 @php
    $route = Route::current()->getName();
 @endphp
 @if($route != 'user.edit')
    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
        {{ Form::label('','Password') }}
        {{ Form::password('password',['class'=>'form-control','placeholder'=>'Ex. x&38djjs9=-_=+']) }}
        @if($errors->has('password'))
            <span class="help-block" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        {{ Form::label('','Confirm-Password') }}
        {{ Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Ex. x&38djjs9=-_=+']) }}
    </div>
@endif

 <div class="form-group {{ $errors->has('role_id') ? 'has-error' : '' }}">
    {{ Form::label('','Role') }}
    {{ Form::select('role_id[]',$roles,$user->roles,['class'=>'form-control select2','multiple']) }}
    @if($errors->has('role_id'))
        <span class="help-block" role="alert">
            <strong>{{ $errors->first('role_id') }}</strong>
        </span>
    @endif
</div>


