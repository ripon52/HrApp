@extends('layouts.admin')
@section('title','Register Role - HR Application')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Role Management</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Role Employee </li>
                    </ol>
                </div><!-- /.col -->


            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->

    <section class="content">
        <div class="container-fluid">
            {{-- Cart Item Start --}}
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Add Role</h3>
                                </div>
                                <div class="card-body">
                                    {{ Form::model($role = new \App\Role(),['route'=>'role.store','method'=>'post','files'=>true]) }}
                                    @include('role.form-role')
                                    {{ Form::submit('Register Role',['class'=>'btn btn-success']) }}
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-12 col-xs-12">
                            {{-- all users retrive start --}}
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Add Role</h3>
                                </div>
                                <div class="card-body table-responsive">
                                    <table class="table table-hover" id="dataTable">
                                        <thead>
                                        <tr>
                                            <th>#SL</th>
                                            <th>Name</th>
                                            <th>Permissions</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @include('role.roles')
                                        </tbody>
                                    </table>
                                </div>
                                {{-- all users retrive end --}}
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#dataTable").DataTable();
        });
    </script>
@endsection
