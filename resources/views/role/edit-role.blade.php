@extends('layouts.admin')
@section('title','Register Role - HR Application')
@section('content')
    <section class="content-header">
        <h1>Dashboard
            <small>Role Management</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-6 col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Role</h3>
                    </div>
                    <div class="box-body">

                        {{ Form::model($role,['route'=>['role.update',$role->id],'method'=>'post','files'=>true]) }}
                         @include('role.form-role')
                        {{ Form::submit('Update Role',['class'=>'btn btn-info']) }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-12">
                {{-- all users retrive start --}}
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">All Roles </h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#SL</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @include('role.roles')
                            </tbody>
                        </table>
                    </div>
                    {{-- all users retrive end --}}
                </div>

            </div>

        </div>


    </section>
    <!-- /.content -->

@endsection
