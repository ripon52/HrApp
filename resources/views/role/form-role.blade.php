<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} ">
    {{ Form::label('','Role Name') }}
    {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Super Admin']) }}

    @if($errors->has('name'))
        <span class="strong">
            <strong class="help-block"> {{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>

<div class="form-group {{ $errors->has('menu_id') ? 'has-error' : '' }}">
    {{ Form::label('','Menu') }}

  {{--  <select class="form-control select2" multiple name="menu_id[] ">
        @foreach($menus as $key=>$menu)
            @foreach(\App\Menu::query()->where('prefix','=',$menu->prefix)->get() as $in=>$info)
                <option value="{{ $info->id }}"> {{ $info->name }} - {{ $info->isEdit }}</option>
            @endforeach
        @endforeach
    </select>--}}
    {{ Form::select('menu_id[]',$menus,$role->menus,['class'=>'form-control select2','multiple'=>'multiple']) }}
    @error('menu_id')
        <span class="strong">
            <strong class="help-block"> {{ $message }}</strong>
        </span>
    @enderror

</div>
