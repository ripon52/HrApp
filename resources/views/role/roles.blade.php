@php $sl=0; @endphp

@foreach($roles as $role)
    <tr>
        <td>{{ ++$sl }}</td>
        <td>{{ $role->name }}</td>
        <td>
            @if($role->menus->count() >0 )

                @foreach($role->menus as $menu)
                    <label class="label label-primary " style="
    background: #000;
    color: #fff;
    padding: 2px 8px;
    border-radius: 9px 0 9px;
"> {{ $menu->name }} </label> &nbsp;
                @endforeach

            @else
                <label class="label label-danger btn-block">No Menus Permission for this role</label>
            @endif

        </td>
        <td>{!!  $role->status == 1 ? "<label class='label label-success'> Active </label>" : "<label class='label label-danger'> In-Active </label>" !!} </td>
        <td>
            <a href="{{ route('role.edit',$role->id) }}" class="btn btn-success fa fa-edit"></a> ||
            <button type="button" data-id="{{ $role->id }}" data-url="{{ route("role.destroy") }}" class="btn btn-danger fa fa-trash erase"></button>
        </td>
    </tr>

@endforeach
