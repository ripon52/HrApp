@extends('layouts.admin')
@section('title','Dashboard - JR TELECOM')
@section('content')
    <!-- Content Wrapper. Contains page content -->
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard v1</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3 style="font-size: 1.1rem;">{{ 0 }} <sup style="font-size: 13px">Tk</sup></h3>

                                <p style="font-size: 0.8rem!important;">Total Purchased Amount</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3 style="font-size: 1.1rem;">{{ 0 }} <sup style="font-size: 13px">Tk</sup></h3>

                                <p style="font-size: 0.8rem!important;">Total Product Sale Amount</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3 style="font-size: 1rem;">{{ 0 }}</h3>

                                <p style="font-size: 0.8rem!important;">Total Customers</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3 style="font-size: 1rem;">{{ 0 }} <sup style="font-size: 13px">Tk</sup></h3>

                                <p style="font-size: 0.8rem!important;">Today's Sale</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-money"></i>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->
                <!-- Main row -->

                </div>
        </section>
        <!-- /.content -->

    {{-- Most SOld and servicing list --}}
    <section class="content">
        <div class="container-fluid">

            {{-- Cart Item Start --}}
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">

                        <div class="card-header bg-dark">
                            <h4 class="card-title text-left">Top 20 Most Popular Product's</h4>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive" style="height: 350px;overflow: scroll;">
                                <table class="table-hover table" id="popularProduct">
                                    <thead>
                                        <tr>
                                            <td> SL </td>
                                            <td> Product </td>
                                            <td> Sold </td>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">

                        <div class="card-header bg-dark">
                            <h4 class="card-title text-left">Top 10 Most Popular Servicing </h4>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive" style="height: 350px;overflow: scroll;">
                                <table class="table-hover table" id="saleTable">
                                    <thead>
                                        <tr>
                                            <td> SL </td>
                                            <td> Service </td>
                                            <td> QTY </td>
                                        </tr>
                                    </thead>

                                    <tbody>


                                    </tbody>

                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- Most SOld and servicing List end --}}

    @endsection



@section('css')

@endsection

@section('js')

    <script>
        $(document).ready(function () {
            var printCounter = 0;

            $("#stockTable").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy',
                    {
                        extend: 'excel',
                        messageTop: 'The information in this table is copyright to Sirius Cybernetics Corp.'
                    },
                    {
                        extend: 'pdf',
                        messageBottom: null
                    },
                    {
                        extend: 'print',
                        messageTop: function () {
                            return 'This is the first time you have printed this document.';
                        },
                    }
                ]
            });

            $("#saleTable").DataTable({
                dom: 'Bfrtip',
                buttons: [
                  'csv', 'excel', 'pdf', 'print'
                ]
            });

            $("#LastsaleTable").DataTable({
                dom: 'Bfrtip',
                buttons: [
                  'csv', 'excel', 'pdf', 'print'
                ]
            });

            $("#popularProduct").DataTable({
                dom:"Bfrtip",
                buttons:[
                    'csv','excel','pdf','print'
                ]
            });

            $("#TodaysaleTable").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ]
            });

            $("#purchaseTable").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ]
            });

            $("#serviceDataTable").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ]
            });



            $("#TodaypurchaseTable").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ]
            });



            $("#customerDues").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ]
            });


        });









    </script>
@endsection
