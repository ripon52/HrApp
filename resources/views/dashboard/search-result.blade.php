<div class="row">

    <div class="col-lg-12">
        <div class="col-lg-12">
            <h5 class="btn-primary text-center" style="padding:5px;"> Product Stock Result :  </h5>

            <strong>Product Search Result Found : {{ $products->count() }}</strong>
            <div class="card-body table-responsive">
                <table class="table-hover table" id="product_search_table">
                    <thead>
                    <tr>
                        <th> SL</th>
                        <th> Name</th>
                        <th> Brand </th>
                        <th> Color </th>
                        <th> Origin</th>
                        <th> Purchased </th>
                        <th> Sold </th>
                        <th> Stock </th>
                        <th> Barcode </th>
                        <th> P.History </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($products->count()>0)
                        @php  $pro_search=0;  @endphp
                        @foreach($products as $product)
                            {{-- {{ dd($sale->sale) }}--}}
                            @php
                                $p_purchased = \App\PurchaseProduct::where('product_id',$product->id)->sum('qty');
                                $p_sold = \App\ProductSale::where('product_id',$product->id)->count();
                                $stock_search = ($p_purchased !=null ? $p_purchased : 0)-($p_sold !=null ? $p_sold : 0);
                            @endphp
                            <tr>
                                <td>{{ ++$pro_search }}</td>
                                <td> {{ $product->name }}</td>
                                <td>{{ $product->brand->name }}</td>
                                <td>{{ $product->color->name }}</td>
                                <td>{{ $product->origin->name }}</td>
                                <td>{{ $p_purchased }}</td>
                                <td>{{ $p_sold }}</td>
                                <td>{{ $stock_search }}</td>
                                <td>
                                    @if($product->saleAbleBarcodes($product->id)->count()>0)
                                        @foreach($product->saleAbleBarcodes($product->id) as $barcode)
                                            <strong data-code="{{ $barcode->code }}" class="barcodeSaleCart btn
                                            btn-success m-1"> {{
                                            $barcode->code }}
                                            </strong>
                                            <br>
                                        @endforeach
                                    @endif
                                </td>
                                <td><a href="{{ route('product.purchase-history',$product->id) }}" class="fa fa-eye
                            btn btn-success"></a> </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" class="text-center bg-danger">No Product Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div class="col-lg-6">



        <h5 class="btn-primary text-center" style="padding:5px;"> Customer Information  </h5>

        <strong>Customer Search Result Found : {{ $customers->count() }}</strong>
        <div class="row">
            <div class="card-body table-responsive">
                <table class="table-hover table" id="customer_search" style="overflow: scroll; height: auto;">
                    <thead>
                        <tr>
                            <th> SL</th>
                            <th> Name</th>
                            <th> Email</th>
                            <th> Phone</th>
                            <th> Member Since</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if(count($customers)>0)
                            @php $cus_servi=0; @endphp
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{ ++$cus_servi }}</td>
                                    <td> {{ $customer->name }}</td>
                                    <td> {{ $customer->email }}</td>
                                    <td> {{ $customer->phone }}</td>
                                    <td>
                                       {{ \Carbon\Carbon::parse
                    ($customer->created_at)->format('M,d-Y ') }} <br>
                                        {{ $customer->created_at->diffForHumans() }}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <p style="background: #000; color: #fff"> No Result For Customer</p>
                        @endif
                    </tbody>
                </table>

            </div>
            <strong style="display: block;background: #fff;height: 4px;width: 100%;"></strong>
        </div>
    </div>

    <div class="col-lg-6">
        <h5 class="btn-primary text-center" style="padding:5px;"> Purchase Information  </h5>
        <strong>Purchase Search Result Found : {{ count($purchase_products) }}</strong>
        <div class="row">
            <div class="col-lg-12 table-responsive">
               <div class="card-body">
                    <table class="table-hover table" id="search_purchaseTable" style="overflow: scroll;height: auto;">
                        <thead>
                            <tr>
                                <th> SL</th>
                                <th> Purchase No</th>
                                <th> Supplier</th>
                                <th> IMEI</th>
                                <th> SERIAL</th>
                                <th> Total </th>
                                <th> Paid </th>
                                <th> Due </th>
                                <th>Status</th>
                                <th>Memo</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if(count($purchase_products)>0)
                                @php $pr=0; @endphp
                                @foreach($purchase_products as $purchase)
                                   {{-- {{ dd($purchase) }}--}}
                                   @php
                                       $search_purchase_total =  \App\Purchase::findOrFail($purchase->purchase_id)->total;
                                       $search_purchse_paid = \App\Cashbook::where('purchase_id',$purchase->id)->sum
                                       ('expense');
                                   @endphp
                                    <tr>
                                        <td>{{ ++$pr }}</td>
                                        <td> {{ $purchase->purchase_no }}</td>
                                        <td> {{ $purchase->supplier_name }}</td>
                                        <td> {{ $purchase->imei }}</td>
                                        <td> {{ $purchase->serial }}</td>
                                        <td> {{ $search_purchase_total  }}</td>
                                        <td> {{ $search_purchse_paid }}</td>
                                        <td> {{ $search_purchase_total-$search_purchse_paid }}  </td>
                                        <td>
                                            {!! $search_purchase_total-$search_purchse_paid == 0 ? "<i class='fas fa-check
                                            fa-2x'></i>"  :
                                            "<i class='fas fa-times-circle fa-2x'></i>" !!}
                                        </td>
                                        <td><a href="#" data-id="{{ $purchase->purchase_id }}" class="fa
                                        fa-money-check-alt  btn btn-danger money_receipts" ></a> </td>
                                    </tr>
                                @endforeach
                             @else
                                 <p style="background: #000; color: #fff"> No Result For Purchase</p>
                            @endif

                        </tbody>

                    </table>

               </div>
                <strong style="display: block;background: #fff;height: 4px;width: 100%;"></strong>
            </div>
        </div>
    </div>

    {{--
        ########## Purchase Model Start
    --}}
    {{-- Purchase Money receipt Modal --}}
    <div class="modal fade  " id="invoice_modal" aria-modal="true" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cash Payment Info</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <td>SL</td>
                                <td>Invoice No</td>
                                <td>Amount</td>
                                <td>Discount</td>
                                <td>Balance</td>
                                <td>Paid</td>
                                <td> Print </td>
                            </tr>
                            </thead>

                            <tbody id="allMoneyReceipts"></tbody>

                        </table>
                    </div>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--
        ########## Purchase Model End
    --}}


    <div class="col-lg-6" style="overflow: hidden;">
        <h5 class="btn-primary text-center" style="padding:5px;"> Sale Information  </h5>

        <strong>Sale Search Result Found : {{ $sales->count() }}</strong>
        <div class="card-body table-responsive">
            <table class="table-hover table" id="sale_search_table">
                <thead>
                <tr>
                    <th> SL</th>
                    <th> Invoice No</th>
                    <th> Name & Phone</th>
                    <th> Amount </th>
                    <th> Discount </th>
                    <th> Paid </th>
                    <th> Due </th>
                    <th>Status</th>
                    <th>Memo</th>
                </tr>
                </thead>

                <tbody id="servicingList">
                @if($sales->count()>0)
                    @foreach($sales as $sale)
                       {{-- {{ dd($sale->sale) }}--}}
                            <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td> {{ $sale->sale_no }}</td>
                            <td>

                                <a href="{{ route('customer.details1',$sale->sale->customer_id) }}" target="_blank"> {{
                                $sale->sale->customer->name." - ".$sale->sale->customer->phone }} </a>

                            </td>
                            <td> {{ $sale->sale->total }}</td>
                            <td> {{ $sale->sale->discount }}</td>
                            <td> {{ $paid = \App\Cashbook::where('sale_id',$sale->id)->sum('income') }}</td>
                            <td> {{ $due = $sale->sale->total-$sale->sale->discount-$paid }} </td>
                            <td>    <i class="{!! $due>0 ? 'fas fa-times-circle fa-2x' : 'fas fa-check-double fa-2x' !!}"> </i>   </td>
                            <td> <a href="#" data-id="{{ $sale->id }}" class="fa fa-money-check-alt btn btn-danger
                            sale_money_receipts" ></a> </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9" class="text-center bg-danger">No  Service Found</td>
                    </tr>
                @endif


                </tbody>

            </table>
        </div>

    </div>


    <div class="col-lg-6">
        <h5 class="btn-primary text-center" style="padding:5px;"> Servicing Information  </h5>

        <strong>Servicing  Search Result Found : {{ $servicings->count() }}</strong>
        <div class="card-body table-responsive">
            <table class="table-hover table" id="service_search_table">
                <thead>
                    <tr>
                        <th> SL</th>
                        <th> Invoice No</th>
                        <th> Serial </th>
                        <th> IMEI </th>
                        <th> Name & Phone</th>
                        <th> Amount </th>
                        <th> Discount </th>
                        <th> Paid </th>
                        <th> Due </th>
                        <th>Status</th>
                        <th>Memo</th>
                    </tr>
                </thead>
                <tbody>
                    @if($servicings->count()>0)
                        @php  $serv_id=0;  @endphp
                        @foreach($servicings as $search_service)
                            {{-- {{ dd($sale->sale) }}--}}
                            <tr>
                                <td>{{ ++$serv_id }}</td>
                                <td> {{ $search_service->invoice->invoice_no }}</td>
                                <td>{{ $search_service->serial }}</td>
                                <td>{{ $search_service->imei }}</td>
                                <td>
                                    <a href="{{ route('customer.details1',$search_service->invoice->customer_id) }}"
                                       target="_blank"> {{
                                    $search_service->invoice->customer->name." - ".$search_service->invoice->customer->phone }} </a>

                                </td>
                                <td> {{ $search_service->invoice->amount }}</td>
                                <td> {{ $search_service->invoice->discount }}</td>
                                <td> {{ $paid = \App\Cashbook::where('invoice_id',$search_service->id)->sum('income')
                                }}</td>
                                <td> {{ $due = $search_service->invoice->amount-$search_service->invoice->discount-$paid }} </td>
                                <td>    <i class="{!! $due>0 ? 'fas fa-times-circle fa-2x' : 'fas fa-check-double fa-2x' !!}"> </i>   </td>
                                <td> <a href="#" data-id="{{ $search_service->invoice_id }}" class="fa fa-money-check-alt
                                btn btn-danger
                                servicing_money_receipts" ></a> </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" class="text-center bg-danger">No  Service Found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>

    </div>






</div>


<script>
    $("#search_purchaseTable").DataTable({
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ]
    });

    $("#sale_search_table").DataTable({
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ]
    });

    $("#product_search_table").DataTable({
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ]
    });

    $("#customer_search").DataTable({
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ]
    });





    $("#service_search_table").DataTable({
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ]
    });

    $(document).on('click','.money_receipts',function (){

        $("#invoice_modal").modal("show");

        var id = $(this).attr('data-id');
        var token = '{{ csrf_token() }}';

        $.ajax({
            method:"post",
            url:"{{ route('purchase.invoices') }}",
            data:{id:id,_token:token},
            dataType:"html",
            success:function(res){
                $('#allMoneyReceipts').html(res);
            }
        });
    });


    /* Sales Invoices */

    $(document).on('click','.sale_money_receipts',function (){

        $("#invoice_modal").modal("show");

        var id = $(this).attr('data-id');
        var token = '{{ csrf_token() }}';

        $.ajax({
            method:"post",
            url:"{{ route('sale.invoices') }}",
            data:{id:id,_token:token},
            dataType:"html",
            success:function(res){
                $('#allMoneyReceipts').html(res);
            }
        });
    });


    $(document).on('click','.servicing_money_receipts',function (){

        $("#invoice_modal").modal("show");

        var id = $(this).attr('data-id');
        var token = '{{ csrf_token() }}';

        $.ajax({
            method:"post",
            url:"{{ route('invoice.log') }}",
            data:{id:id,_token:token},
            dataType:"html",
            success:function(res){
                $('#allMoneyReceipts').html(res);
            }
        });
    });

</script>
