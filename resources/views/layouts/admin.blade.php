<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('admin/css/ionicons.min.css') }}">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admin/dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css')}}">
    <!-- Google Font: Source Sans Pro -->
{{--    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">--}}

    <link rel="stylesheet" href="{{ asset('admin/css/sweetalert.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/animate.min.css') }}">

    <link href="{{ asset('admin/plugins/select2/css/select2.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <style>

        body{
            font-family:"ubuntu";
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice{
            background-color: #055227!important;
        }

        .invoiceTableA{}


        .invoiceTableA tbody  tr td{
            line-height: 10px;
        }
    </style>
    @yield('css')

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Navbar -->
    @include('include.header')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->

        <a href="#" class="brand-link">
            <img src="{{ asset('admin/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle
            elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">HR Application</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User
                    Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block"> {{ Auth::user()->name }} </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    @include('include.admin-menu')
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="col-lg-12 search_result">

                </div>
            </div>
        </section>
        @yield('content')
    </div>

    @include('include.footer')

    <script>


    </script>

    {{-- Session part --}}

    @if (session()->has('success'))
        <script type="text/javascript">
            $(function () {
                $.notify("{{session()->get("success")}}", {globalPosition: 'bottom right',className: 'success'});
            });
        </script>
    @endif


    @if (session()->has('repair'))
        <script type="text/javascript">
            $(function () {
                var url = '{{ route("repair.last-repaire-invoice-print") }}';
                window.open(url);
            });
        </script>
    @endif




    @if (session()->has('error'))
        <script type="text/javascript">
            $(function () {
                $.notify("{{session()->get("error")}}", {globalPosition: 'bottom right',className: 'error'});
            });
        </script>
    @endif

    @if (session()->has('warning'))
        <script type="text/javascript">
            $(function () {
                $.notify("{{session()->get("warning")}}", {globalPosition: 'bottom right',className: 'warn'});
            });
        </script>
    @endif




    {{-- Item Deletion start --}}
    <script>

        $(document).ready(function(){
            $("body").attr("class","sidebar-collapse");
            setInterval(function(){
                var status = navigator.onLine;
                if (status) {
                    $("#online").removeClass('btn-danger');
                    $("#online").text("Online");
                } else {
                    $("#online").text("Offline");
                    console.log("Not Connected");
                }
            },1000);
    });

        $(document).on('click', '.erase', function () {
            var id = $(this).attr('data-id');
            var url=$(this).attr('data-url');
            console.log("Clicked ID : "+id+ " Request URL : "+url);
            var token = '{{csrf_token()}}';
            var $tr = $(this).closest('tr');
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this information!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: "No, cancel plz!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: url,
                            type: "post",
                            data: {id: id, _token: token},
                            dateType:'html',
                            success: function (response) {
                                swal("Deleted!", "Data has been Deleted.", "success"),
                                    swal({
                                            title: "Deleted!",
                                            text: "Data has been Deleted.",
                                            type: "success"
                                        },
                                        function (isConfirm) {
                                            if (isConfirm) {
                                                $tr.find('td').fadeOut(1000, function () {
                                                    $tr.remove();
                                                    servicing();
                                                });
                                            }
                                        });
                            }
                        });
                    } else {
                        swal("Cancelled", "Your data is safe :)", "error");
                    }
                });

        });

        $(document).on('click', '.erase1', function () {
            var id = $(this).attr('data-id');
            var url=$(this).attr('data-url');
            console.log("Clicked ID : "+id+ " Request URL : "+url);
            var token = '{{csrf_token()}}';
            var $tr = $(this).closest('tr');
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this information!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: "No, cancel plz!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: url,
                            type: "post",
                            data: {id: id, _token: token},
                            dateType:'html',
                            success: function (response) {
                                swal("Deleted!", "Data has been Deleted.", "success"),
                                    swal({
                                            title: "Deleted!",
                                            text: "Data has been Deleted.",
                                            type: "success"
                                        },
                                        function (isConfirm) {
                                            if (isConfirm) {
                                                $tr.find('td').fadeOut(1000, function () {
                                                    $tr.remove();
                                                });
                                            }
                                        });
                            }
                        });
                    } else {
                        swal("Cancelled", "Your data is safe :)", "error");
                    }
                });

        });
    </script>
    {{-- Item Deletion End --}}

</div>

@yield('script')
@yield('js')

</body>
</html>
