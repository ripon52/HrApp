<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/fontawesome-free-5.6.3-web/css/all.min.css') }}">
    <!-- Nano Scroller -->
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/nanoScroller/nanoscroller.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Bootstrap 4 -->
    <link href="{{ asset('dashboard/plugins/bootstrap/css/bootstrap.min.css') }}" />
    <!-- Data-Table Css -->
    <link href="{{ asset('dashboard/plugins/datatables/dataTables.bootstrap4.css')}}" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

    {{-- Datepicker CSS --}}
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/datepicker/datepicker3.css') }}">
    {{-- Date Range Picker--}}
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/daterangepicker/daterangepicker-bs3.css') }}" />


    <!-- Sweet alert css -->
    <link rel="stylesheet" href="{{ asset('css/sweetalert.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">

    @yield('plugin-css')


    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dashboard/dist/css/adminlte.min.css?ver:1.1') }}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    @yield('style')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        @include('includes.header')
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
    @include('includes.left-sidebar')
    <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        @include('includes.footer')
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        @include('includes.right-aside')
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('dashboard/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Nano Scroller -->
<script src="{{ asset('dashboard/plugins/nanoScroller/jquery.nanoscroller.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dashboard/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dashboard/dist/js/demo.js') }}"></script>

{{-- datepicker--}}

<script src="{{ asset('public/plugins/daterangepicker/moment.js') }}"></script>
<script src="{{ asset('dashboard/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('dashboard/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!--sweetalert Js -->
<script src="{{ asset('js/notify.min.js') }}"></script>
<script src="{{ asset('js/sweetalert.min.js') }}"></script>

<!-- Data-table Js -->

<script src="{{ asset('dashboard/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"> </script>

<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

@yield('plugin')

@yield('script')

<script>

$(document).ready(function(){

    $("body").attr('class','sidebar-mini sidebar-collapse');

    $('.date').datepicker();

    $('#daterange').daterangepicker();

    $('#datatable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });


});

$(document).on('click', '.erase', function () {
    var id = $(this).attr('data-id');
    var url=$(this).attr('data-url');
    console.log("Clicked ID : "+id+ " Request URL : "+url);
    var token = '{{csrf_token()}}';
    var $tr = $(this).closest('tr');
    swal({
            title: "Are you sure?",
            text: "You will not be able to recover this information!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: "No, cancel plz!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: url,
                    type: "post",
                    data: {id: id, _token: token},
                    dateType:'html',
                    success: function (response) {
                        swal("Deleted!", "Data has been Deleted.", "success"),
                            swal({
                                    title: "Deleted!",
                                    text: "Data has been Deleted.",
                                    type: "success"
                                },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        $tr.find('td').fadeOut(1000, function () {
                                            $tr.remove();
                                        });
                                    }
                                });
                    }
                });
            } else {
                swal("Cancelled", "Your data is safe :)", "error");
            }
        });
      });
  //  $(".nano").nanoScroller({
    //    preventPageScrolling: true,
  //  });

</script>


<!-- ALL Session Javascript Fire start  -->
    @if(session()->has('success'))
        <script type="text/javascript">
            $(function () {
                $.notify("{{session()->get("success")}}", {globalPosition: 'bottom right',className: 'success'});
            });
        </script>
    @endif

    @if (session()->has('error'))
        <script type="text/javascript">
            $(function () {
                $.notify("{{session()->get("error")}}", {globalPosition: 'bottom right',className: 'error'});
            });
        </script>
    @endif

    @if (session()->has('warning'))
        <script type="text/javascript">
            $(function () {
                $.notify("{{session()->get("warning")}}", {globalPosition: 'bottom right',className: 'warn'});
            });
        </script>
    @endif

<!-- ALL Session Javascript Fire End -->


</body>
</html>
