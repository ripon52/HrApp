
<div class="col-lg-4">
    <div class="form-group @error('designation_id') has-error @enderror ">
        {{ Form::label('','Designation *') }}
        <select name="find_designation_id" class="form-control" id="">
            @foreach($designations as $key=>$designation)
                <option value="{{ encrypt($designation->designation_id) }}">{{ $designation->designation->name }}</option>
            @endforeach
        </select>

        @error('designation_id')
        <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="col-lg-4">
    <div class="form-group @error('join_date') has-error @enderror ">
        {{ Form::label('','Attendance Date *') }}
        {{ Form::date('date',isset($_GET['date']) ? $_GET['date'] : null,['class'=>'form-control','required',
        'placeholder'=>'Ex
        .2020-06-21']) }}

        @error('join_date')
        <span class="strong">
                <strong class="help-block"> {{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>


<div class="col-lg-4">
    <div class="form-group " style="padding-top: 32px;">
        {{ Form::submit('Find Employees',['class'=>'btn btn-success']) }}
    </div>
</div>
