@extends('layouts.admin')
@section('title','ALL Attendance - HR Application')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Attendance Management</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">View Attendance </li>
                    </ol>
                </div><!-- /.col -->

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{ route('attendance.add') }}" class="btn btn-success pull-right"> New
                                Attendance</a>

                        </div>
                    </div>
                </div>


            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->

    <section class="content">
        <div class="container-fluid">
            {{-- Cart Item Start --}}
            <div class="row">
                <!-- ./col -->

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Filter Attendance</h3>
                        </div>
                        <div class="card-body">
                            {{ Form::open(['route'=>'attendance.view','method'=>'get']) }}
                            <div class="row">
                                @include('attendance.form-attendance')
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>


                <div class="col-lg-12 col-xs-12">
                    {{-- all users retrive start --}}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">ALL Attendance</h3>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-hover" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>#SL</th>
                                        <th>Avatar</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Date</th>
                                        <th>Enter</th>
                                        <th>Leave</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                @if(isset($_GET['find_designation_id']))
                                    @if($attendances->count()>0)
                                        @foreach($attendances as $key=>$employee)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>
                                                    <img src="{{ $employee->avatar !=null ? asset('upload/employee/'
                                                    .$employee->avatar) : asset('upload/noimage.png') }}"
                                                         style="height:
                                                    100px;width: 100px" alt="">
                                                </td>
                                                <td>{{ $employee->employee->name }}</td>

                                                <td>{{ $employee->designation->name }}</td>
                                                <td>{{ $employee->date }}</td>
                                                <td>
                                                    @php
                                                        $timeEx = explode(":",$employee->enter_time);
                                                        $leaveTime = explode(":",$employee->leave_time);
                                                    @endphp
                                                    {{ $employee->enter_time }}
                                                    {{ $employee->enter_time!=null && $timeEx[0]>12 ||
                                                    $timeEx[0]==12 ? 'PM' : 'AM' }}
                                                </td>
                                                <td>{{ $employee->leave_time }} {{ $employee->leave_time!=null &&
                                                $leaveTime[0]>12 ||
                                                $leaveTime[0]==12 ? 'PM' : 'AM' }}</td>
                                                <td>

                                                    <a href="{{ route('attendance.edit',encrypt( $employee->id)) }}" class="fa
                                                    fa-edit bg-success ml-2 p-2
                                                    rounded"></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @else
                                    @if($attendances->count()>0)
                                        @foreach($attendances as $key=>$employee)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>
                                                    <img src="{{ $employee->avatar !=null ? asset('upload/employee/'
                                                    .$employee->avatar) : asset('upload/noimage.png') }}"
                                                         style="height:
                                                    100px;width: 100px" alt="">
                                                </td>
                                                <td>{{ $employee->employee->name }}</td>

                                                <td>{{ $employee->designation->name }}</td>
                                                <td>{{ $employee->date }}</td>
                                                <td>
                                                    @php
                                                        $timeEx = explode(":",$employee->enter_time);
                                                        $leaveTime = explode(":",$employee->leave_time);
                                                    @endphp
                                                    {{ $employee->enter_time }}
                                                    {{ $employee->enter_time!=null && $timeEx[0]>12 ||
                                                    $timeEx[0]==12 ? 'PM' : 'AM' }}
                                                </td>
                                                <td>{{ $employee->leave_time }} {{ $employee->leave_time!=null &&
                                                $leaveTime[0]>12 ||
                                                $leaveTime[0]==12 ? 'PM' : 'AM' }}</td>
                                                <td>

                                                    <a href="{{ route('attendance.edit',encrypt( $employee->id)) }}" class="fa
                                                    fa-edit bg-success ml-2 p-2
                                                    rounded"></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endif
                                </tbody>
                            </table>
                        </div>
                        {{-- all users retrive end --}}
                    </div>

                </div>

            </div>


    </section>
    <!-- /.content -->

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#dataTable").DataTable();
        });
    </script>
@endsection
