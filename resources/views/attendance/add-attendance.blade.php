@extends('layouts.admin')

@section('title','New Attendance | HR Application')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Employee Attendance</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">New Attendance </li>
                    </ol>
                </div><!-- /.col -->

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{ route('attendance.view') }}" class="btn btn-success pull-right"> ALL
                                Attendances</a>
                        </div>
                    </div>

                </div>

            </div><!-- /.row -->


        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->

    <section class="content">
        <div class="container-fluid">
            {{-- Cart Item Start --}}
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">New Attendance</h3>
                        </div>
                        <div class="card-body">
                            {{ Form::open(['route'=>'attendance.add','method'=>'get']) }}
                            <div class="row">
                                @include('attendance.form-attendance')
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Employee List </h3>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table-hover table">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Designation</th>
                                            <th>Enter Time</th>
                                            <th>Leave Time</th>
                                        </tr>
                                    </thead>

                                    <tbody>


                            {{ Form::open(['route'=>'attendance.store','method'=>'post']) }}

                              @if($employees !=null)
                                @foreach($employees as $key=>$employee)

                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        {{ $employee->name }}
                                        {{ Form::hidden('employee_id[]',$employee->id) }}
                                        {{ Form::hidden('date',$_GET['date']) }}
                                    </td>

                                    <td>  {{ $employee->designation->name }}      </td>

                                    <td>
                                        <input
                                               type="time"
                                               class="form-control"
                                               name="enter_time[]"
                                                required
                                        >
                                    </td>
                                    <td>
                                         <input
                                            type="time"
                                            class="form-control"
                                            name="leave_time[]" />
                                    </td>

                                @endforeach

                              @endif


                                    </tbody>

                                    <tfoot>
                                        <tr>
                                            <td class="text-center" colspan="5">
                                                {{ Form::submit('Save Attendance',['class'=>'btn btn-info']) }}
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <!-- /.content -->

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#dataTable").DataTable();
        });
    </script>
@endsection

